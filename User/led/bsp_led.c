#include "./led/bsp_led.h"


void LED_GPIO_Config( void )
{
	GPIO_InitTypeDef GPIO_InitStructing;
	
	RCC_APB2PeriphClockCmd(LED_CLK, ENABLE);
	GPIO_InitStructing.GPIO_Pin=LED_PIN ;
	GPIO_InitStructing.GPIO_Mode=GPIO_Mode_Out_PP;
	GPIO_InitStructing.GPIO_Speed=GPIO_Speed_50MHz;
	
	GPIO_Init(LED_PORT,&GPIO_InitStructing);
	
	GPIO_SetBits(LED_PORT,LED_PIN);
}






