#ifndef _BSP_LED_H
#define _BSP_LED_H

#include "stm32f10x.h"


#define LED_PIN      GPIO_Pin_9
#define LED_PORT     GPIOB
#define LED_CLK      RCC_APB2Periph_GPIOB

#define LED_ON       GPIO_ResetBits(LED_PORT,LED_PIN)
#define LED_OFF      GPIO_SetBits(LED_PORT,LED_PIN)

#define LED1_TOGGLE  {LED_PORT->ODR^=LED_PIN;}

void LED_GPIO_Config( void );



#endif /*_BSP_LED_H*/



